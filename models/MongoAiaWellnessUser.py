from mongoengine import *
import datetime


# AiaWellnessUser
class AiaWellnessUser(Document):
    constructor = DictField(required=True)
    first_name = StringField(max_length=30, required=True)
    last_name = StringField(max_length=30, required=True)
    telephone_no = StringField(max_length=20, required=True)  # saved with the plus sign
    # nic = StringField(max_length=12, required=True)
    email = StringField(max_length=250, required=True)
    created_date = DateTimeField(default=datetime.datetime.now)
