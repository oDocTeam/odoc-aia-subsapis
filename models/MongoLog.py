from mongoengine import *
import datetime


# To log API calls
class MongoLog(Document):
    API_name = StringField(required=True, max_length=30)
    request = DictField(default={})
    response = DictField(required=True, default={})
    timestamp = DateTimeField(default=datetime.datetime.now)
