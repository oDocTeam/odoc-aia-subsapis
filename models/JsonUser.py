class JsonUser:
    def __init__(self, first_name, last_name, telephone_no, email, approved_status, created_date):
        self.first_name = first_name
        self.last_name = last_name
        self.telephone_no = telephone_no
        self.email = email
        self.approved_status = approved_status
        self.created_date = created_date
