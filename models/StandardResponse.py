class StandardResponse(object):
    def __init__(self, success, error_message=None, success_message=None):
        self.success = success
        self.error_message = error_message
        self.success_message = success_message

    def to_dict(self):
        return {
            "success": self.success,
            "error_message": self.error_message,
            "success_message": self.success_message
        }
