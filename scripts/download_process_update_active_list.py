import sys
import os.path
current_path = os.path.abspath(os.path.dirname(__file__))
path = os.path.join(current_path, "..")
sys.path.append(path)

from services.FileService import FileService
from services.ActiveListService import ActiveListService

FileService.download_processed_archive()

FileService.decrypt_processed_archive()
ActiveListService.update_active_list()
ActiveListService.update_active_email_list()
ActiveListService.create_json_file()

FileService.decrypt_processed_archive_aia_wellness()
ActiveListService.update_active_list_aia_wellness()
ActiveListService.update_active_email_list_aia_wellness()
ActiveListService.create_json_file_aia_wellness()