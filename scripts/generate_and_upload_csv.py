import sys
import os.path
current_path = os.path.abspath(os.path.dirname(__file__))
path = os.path.join(current_path, "..")
sys.path.append(path)

from services.FileService import FileService

FileService.generate_approval_csv()
FileService.encrypt_approval_csv()

FileService.generate_approval_csv_aia_wellness()
FileService.encrypt_approval_csv_aia_wellness()

FileService.upload_approval_archive()






