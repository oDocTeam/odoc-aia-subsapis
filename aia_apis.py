#!/usr/bin/env python
from flask import Flask
from flask import request
from flask import make_response
from pprint import pprint
import json
import os
import settings
from services.UserService import UserService
from services.LogService import LogService
from services.ActiveListService import ActiveListService
from services.FileService import FileService
from models.StandardResponse import StandardResponse

# connect to mongo
settings.connect_to_mongo()


# TODO: Services should return Python object. Interfacepoints (e.g. this b2c_apis class) should convert to Flask
# TODO: Create a 'verbose' logging flag (for pprints)
# TODO: Link to read for MongoEngine debugging
# http://docs.mongoengine.org/projects/flask-mongoengine/en/latest/

# Flask app should start in global layout
app = Flask(__name__)


@app.route('/test', methods=['GET'])
def test():
    if request.method == 'GET':
        resp = make_response("Test was successful!", 200)
    else:
        resp = make_response("Something other than GET...", 500)
    return resp


# Website APIs
# AIA Clients
@app.route('/user', methods=['POST'])
def api_create_user():
    if request.method == 'POST':
        payload = json.loads(request.data)
        response = UserService.create(payload=payload)

        # ---------------------------------------------------------------
        # ---------------------------------------------------------------
        # remove this line when AIA starts their verification process
        # ActiveListService.add_all_users_to_active_list()
        # ---------------------------------------------------------------
        # ---------------------------------------------------------------

        LogService.log_data(api_name='/user', request=payload, response=response.to_dict())
        return make_response(json.dumps(response.to_dict()), 200)
    else:
        resp = make_response("Something other than POST...", 500)
    return resp


# AIA Wellness
@app.route('/user-aia-wellness', methods=['POST'])
def api_create_user_aia_wellness():
    if request.method == 'POST':
        payload = json.loads(request.data)
        response = UserService.create_aia_wellness(payload=payload)

        LogService.log_data(api_name='/user-aia-wellness', request=payload, response=response.to_dict())
        return make_response(json.dumps(response.to_dict()), 200)
    else:
        resp = make_response("Something other than POST...", 500)
    return resp


# Testing APIs (should be exposed only on dev)
@app.route('/test-dump', methods=['GET'])
def api_dump():
    error_message = None
    success_message = None

    if request.method == 'GET':
        try:
            UserService.dump_database()
            success = True
            success_message = "Done"
        except Exception as e:
            success = False
            error_message = "Something unexpected in /test-all. Check logs for error = " + str(e)
    else:
        success = False
        error_message = "Something other than GET..."

    response = StandardResponse(success=success, success_message=success_message, error_message=error_message)
    return make_response(json.dumps(response.to_dict()), 200)


@app.route('/test-all', methods=['GET'])
def api_all():
    error_message = None
    success_message = None

    if request.method == 'GET':
        try:
            FileService.generate_approval_csv()
            FileService.upload_approval_csv()
            FileService.download_processed_csv()
            ActiveListService.update_active_list()
            success = True
            success_message = "Done"
        except Exception as e:
            success = False
            error_message = "Something unexpected in /test-all. Check logs for error = " + str(e)
    else:
        success = False
        error_message = "Something other than GET..."

    response = StandardResponse(success=success, success_message=success_message, error_message=error_message)
    return make_response(json.dumps(response.to_dict()), 200)


@app.route('/test-generate', methods=['GET'])
def api_generate():
    error_message = None
    success_message = None

    if request.method == 'GET':
        try:
            FileService.generate_approval_csv()
            success = True
            success_message = "Done"
        except Exception as e:
            success = False
            error_message = "Something unexpected in /test-generate. Check logs for error = " + str(e)
    else:
        success = False
        error_message = "Something other than GET..."

    response = StandardResponse(success=success, success_message=success_message, error_message=error_message)
    return make_response(json.dumps(response.to_dict()), 200)


@app.route('/test-upload', methods=['GET'])
def api_upload():
    error_message = None
    success_message = None

    if request.method == 'GET':
        try:
            FileService.upload_approval_csv()
            success = True
            success_message = "Done"
        except Exception as e:
            success = False
            error_message = "Something unexpected in /test-upload. Check logs for error = " + str(e)
    else:
        success = False
        error_message = "Something other than GET..."

    response = StandardResponse(success=success, success_message=success_message, error_message=error_message)
    return make_response(json.dumps(response.to_dict()), 200)


@app.route('/test-download', methods=['GET'])
def api_download():
    error_message = None
    success_message = None

    if request.method == 'GET':
        try:
            FileService.download_processed_csv()
            success = True
            success_message = "Done"
        except Exception as e:
            success = False
            error_message = "Something unexpected in /test-download. Check logs for error = " + str(e)
    else:
        success = False
        error_message = "Something other than GET..."

    response = StandardResponse(success=success, success_message=success_message, error_message=error_message)
    return make_response(json.dumps(response.to_dict()), 200)


@app.route('/test-update', methods=['GET'])
def api_update():
    error_message = None
    success_message = None

    if request.method == 'GET':
        try:
            ActiveListService.update_active_list()
            success = True
            success_message = "Done"
        except Exception as e:
            success = False
            error_message = "Something unexpected in /test-update. Check logs for error = " + str(e)
    else:
        success = False
        error_message = "Something other than GET..."

    response = StandardResponse(success=success, success_message=success_message, error_message=error_message)
    return make_response(json.dumps(response.to_dict()), 200)


if __name__ == '__main__':
    port = int(os.getenv('PORT', settings.flask_port))

    print("Starting app on port %d" % port)

    app.run(debug=True, port=port, host='0.0.0.0')
