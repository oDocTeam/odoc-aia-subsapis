#!/bin/bash

#Current Folder on local server:
current_folder="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
#Destination Folder on remote server:
dest_folder="/home/ubuntu/AIA"
#File to copy to remote server:
file_to_copy="ODOC_INPUT.zip"
file_to_copy_aia_wellness="ODOC_AIA_WELLNESS_INPUT.zip"
# Private key to authenticate to remote server:
pvt_key="~/.ssh/oDoc-AIA-FTP-Server.pem"
# Destination Server Hostname or IP:
dest_server="ubuntu@18.140.91.168"

while getopts k:s:d:a:h? flag ;
do
  case $flag in
    d)
      dest_folder=$OPTARG
     ;;
    k)
      # age of messages (in minutes) to be migrated
      pvt_key=$OPTARG
     ;;
    s)
      dest_server=$OPTARG
     ;;
    h|?)
       echo >&2 "Usage: $0  [-k <private key> |-d <destination folder> ]"
      exit 0
      ;;
  esac
done

ssh_cmd=$(echo "ssh -i $pvt_key $dest_server")
day=$(date +"%d-%m-%Y")
folder=$(echo "${dest_folder}/${day}/")

ssh -i $pvt_key $dest_server date

if [ $? -eq 0 ] ; then
	echo "Successfully connected to remote server!"
else
	echo "ERROR: Unable to connect to remote server...exit" > /dev/stderr
	exit
fi
$ssh_cmd [ -d $folder ]
if [ $? -ne 0 ]  ; then
	echo "$folder did not exist... creating it"

else
	echo "Folder already exists on server... doing nothing"
	#$ssh_cmd "rm -r $folder"
fi
$ssh_cmd mkdir -p $folder
[ $? -eq 0 ]  && echo "Folder created succefully on remote server"
scp -i $pvt_key $current_folder/${day}/$file_to_copy $dest_server:$folder
scp -i $pvt_key $current_folder/${day}/$file_to_copy_aia_wellness $dest_server:$folder
if [ $? -eq 0 ] ; then
	echo "Folder copied successfully"
else
	echo "SCP error"
fi
