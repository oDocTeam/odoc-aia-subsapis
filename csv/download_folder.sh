#!/bin/bash

#Current Folder on local server:
current_folder="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
#Destination Folder on remote server:
dest_folder="/home/ubuntu/AIA"
#File to copy from remote server:
file_to_copy="ODOC_OUTPUT.ZIP"
file_to_copy_aia_wellness="ODOC_AIA_WELLNESS_OUTPUT.ZIP"
# Private key to authenticate to remote server:
pvt_key="~/.ssh/oDoc-AIA-FTP-Server.pem"
# Destination Server Hostname or IP:
dest_server="ubuntu@18.140.91.168"

while getopts k:s:d:a:h? flag ;
do
  case $flag in
    d)
      dest_folder=$OPTARG
     ;;
    k)
      # age of messages (in minutes) to be migrated
      pvt_key=$OPTARG
     ;;
    s)
      dest_server=$OPTARG
     ;;
    h|?)
       echo >&2 "Usage: $0  [-k <private key> |-d <destination folder> ]"
      exit 0
      ;;
  esac
done

ssh_cmd=$(echo "ssh -i $pvt_key $dest_server")
day=$(date +"%d-%m-%Y")
folder=$(echo "${dest_folder}/${day}")

ssh -i $pvt_key $dest_server date

if [ $? -eq 0 ] ; then 
	echo "Successfully connected to remote server!"
else 
	echo "ERROR: Unable to connect to remote server...exit" > /dev/stderr
	exit
fi

if [ ! -d  $day ]  ; then
	echo "$day did not exist... creating it"
	
else
	echo "Folder already exists locally... doing nothing"
	#rm -r $day
fi
#mkdir -p $day
[ $? -eq 0 ]  && echo "Folder created succefully on local server"
scp -i $pvt_key  $dest_server:$folder/$file_to_copy $current_folder/${day}/
scp -i $pvt_key  $dest_server:$folder/$file_to_copy_aia_wellness $current_folder/${day}/
if [ $? -eq 0 ] ; then
	echo "Folder copied successfully"
else
	echo "SCP error"
fi

