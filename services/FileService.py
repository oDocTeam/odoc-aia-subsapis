import csv
import datetime
import os
import subprocess
from pprint import pprint
from zipfile import ZipFile

from services.UserService import UserService

import settings


class FileService(object):
    # the role of the FileService is to:
    # 1 - generate the CSV to be shared with AIA (in the project's base directory: $BASEDIR/csv)
    # 2 - upload the CSV to the FTP server (shared with AIA)
    # 3 - download the processed CSV from the FTP server (shared with AIA)
    # 4 - parse the AIA-processed CSV

    def __init__(self):
        print("Initialized the FileService.")

    @staticmethod
    def get_project_root():
        return os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))

    @staticmethod
    def get_daily_directory():
        return datetime.datetime.today().strftime('%d-%m-%Y')

    @staticmethod
    def create_directory_if_not_exists(path):
        if not os.path.exists(path):
            os.makedirs(path)

    @staticmethod
    def generate_approval_csv():
        # query user's NICs from the UserService
        # user_nic_list = UserService.get_user_nic_list(delimiter="#")
        # pprint("user_nic_list = ")
        # pprint(user_nic_list)

        # query user's phone numbers from the UserService
        user_phone_list= UserService.get_user_phone_list(delimiter="#")
        pprint("user_phone_list = ")
        pprint(user_phone_list)

        # generate the CSV filepath and filename
        csv_filepath = FileService.get_project_root() + settings.kCSV_Directory + "/" + FileService.get_daily_directory()
        csv_filename = csv_filepath + "/" + settings.kCSV_AIA_InputFile

        # create directory if it doesn't exist:
        FileService.create_directory_if_not_exists(csv_filepath)

        # generate the directory name
        # write the CSV to the required (local) directory
        with open(csv_filename, 'w', newline='') as output_file:
            writer = csv.writer(output_file)
            writer.writerows(user_phone_list)


    @staticmethod
    def generate_approval_csv_aia_wellness():
        # query user's NICs from the UserService
        # user_nic_list = UserService.get_user_nic_list(delimiter="#")
        # pprint("user_nic_list = ")
        # pprint(user_nic_list)

        # query user's phone numbers from the UserService
        user_phone_list = UserService.get_aia_wellness_user_phone_list(delimiter="#")
        pprint("aia_wellness_user_phone_list = ")
        pprint(user_phone_list)

        # generate the CSV filepath and filename
        csv_filepath = FileService.get_project_root() + settings.kCSV_Directory + "/" + FileService.get_daily_directory()
        csv_filename = csv_filepath + "/" + settings.kCSV_AIA_Wellness_InputFile

        # create directory if it doesn't exist:
        FileService.create_directory_if_not_exists(csv_filepath)

        # generate the directory name
        # write the CSV to the required (local) directory
        with open(csv_filename, 'w', newline='') as output_file:
            writer = csv.writer(output_file)
            writer.writerows(user_phone_list)

    @staticmethod
    def upload_approval_archive():
        # generate the upload script path
        upload_script_filepath = FileService.get_project_root() + settings.kCSV_Directory + "/" + settings.kCSV_UploadScript

        # upload the CSV to the required (remote) directory
        subprocess.call(upload_script_filepath)

    @staticmethod
    def download_processed_archive():
        # create the CSV folder for download, if it did not exist
        csv_filepath = FileService.get_project_root() + settings.kCSV_Directory + "/" + FileService.get_daily_directory()
        FileService.create_directory_if_not_exists(csv_filepath)

        # generate the download script path
        download_script_filepath = FileService.get_project_root() + settings.kCSV_Directory + "/" + settings.kCSV_DownloadScript

        # download the CSV from the required (remote) directory
        subprocess.call(download_script_filepath)

    @staticmethod
    def decrypt_processed_archive():
        # unarchive the csv (ODOC_OUTPUT.zip) to ODOC_OUTPUT.CSV (password protected)
        archive_filepath = FileService.get_project_root() + settings.kCSV_Directory + "/" + FileService.get_daily_directory() + "/" + settings.kCSV_AIA_OutputFile_archived
        csv_filepath = FileService.get_project_root() + settings.kCSV_Directory + "/" + FileService.get_daily_directory() + "/"
        password = 'Odoc#4409'
        with ZipFile(archive_filepath) as zipArch:
            zipArch.extractall(csv_filepath, pwd=bytes(password, 'utf-8'))

    @staticmethod
    def decrypt_processed_archive_aia_wellness():
        # unarchive the csv (ODOC_AIA_WELLNESS_OUTPUT.zip) to ODOC_AIA_WELLNESS_OUTPUT.CSV (password protected)
        archive_filepath = FileService.get_project_root() + settings.kCSV_Directory + "/" + FileService.get_daily_directory() + "/" + settings.kCSV_AIA_Wellness__OutputFile_archived
        csv_filepath = FileService.get_project_root() + settings.kCSV_Directory + "/" + FileService.get_daily_directory() + "/"
        password = 'Odoc#4409'
        with ZipFile(archive_filepath) as zipArch:
            zipArch.extractall(csv_filepath, pwd=bytes(password, 'utf-8'))

    @staticmethod
    def encrypt_approval_csv():
        # archive the csv (ODOC_INPUT.CSV) to ODOC_INPUT.ZIP (password protected)
        archive_filepath = FileService.get_project_root() + settings.kCSV_Directory + "/" + FileService.get_daily_directory() + "/" + settings.kCSV_AIA_InputFile_archived
        csv_filepath = FileService.get_project_root() + settings.kCSV_Directory + "/" + FileService.get_daily_directory() + "/" + settings.kCSV_AIA_InputFile
        password = 'Odoc#4409'
        rc = subprocess.call(['7z', 'a', archive_filepath, csv_filepath, "-p{}".format(password)])

    @staticmethod
    def encrypt_approval_csv_aia_wellness():
        # archive the csv (ODOC_AIA_WELLNESS_INPUT.CSV) to ODOC_AIA_WELLNESS_INPUT.ZIP (password protected)
        archive_filepath = FileService.get_project_root() + settings.kCSV_Directory + "/" + FileService.get_daily_directory() + "/" + settings.kCSV_AIA_Wellness_InputFile_archived
        csv_filepath = FileService.get_project_root() + settings.kCSV_Directory + "/" + FileService.get_daily_directory() + "/" + settings.kCSV_AIA_Wellness_InputFile
        password = 'Odoc#4409'
        rc = subprocess.call(['7z', 'a', archive_filepath, csv_filepath, "-p{}".format(password)])

    @staticmethod
    def extract_approved_nics():
        # generate the CSV filepath and filename
        csv_filepath = FileService.get_project_root() + settings.kCSV_Directory + "/" + FileService.get_daily_directory()
        csv_filename = csv_filepath + "/" + settings.kCSV_AIA_OutputFile

        approved_nic_list = list()
        # open the CSV from the required (local) directory
        with open(csv_filename, newline='') as input_file:
            csv_reader = csv.reader(input_file, delimiter=',')

            # parse row
            for row in csv_reader:
                # pprint(row)
                approved = row[1].strip()
                approved.replace(r'\r', '')

                if approved == "Y":
                    approved_nic = row[0].strip()
                    approved_nic.replace(r'\r', '')
                    pprint("approving NIC: " + approved_nic)
                    approved_nic_list.append(row[0].strip())

        pprint("Approved NICs:")
        pprint(approved_nic_list)
        return approved_nic_list

    @staticmethod
    def extract_approved_phone_numbers():
        # generate the CSV filepath and filename
        csv_filepath = FileService.get_project_root() + settings.kCSV_Directory + "/" + FileService.get_daily_directory()
        csv_filename = csv_filepath + "/" + settings.kCSV_AIA_OutputFile

        approved_phone_list = list()
        # open the CSV from the required (local) directory
        with open(csv_filename, newline='') as input_file:
            csv_reader = csv.reader(input_file, delimiter=',')

            # parse row
            for row in csv_reader:
                # pprint(row)
                approved = row[1].strip()
                approved.replace(r'\r', '')

                if approved == "Y":
                    approved_phone = row[0].strip()
                    approved_phone.replace(r'\r', '')
                    pprint("approving Phone Number: " + approved_phone)
                    approved_phone_list.append(row[0].strip())

        pprint("Approved Phone Numbers:")
        pprint(approved_phone_list)
        return approved_phone_list

    @staticmethod
    def extract_approved_phone_numbers_aia_wellness():
        # generate the CSV filepath and filename
        csv_filepath = FileService.get_project_root() + settings.kCSV_Directory + "/" + FileService.get_daily_directory()
        csv_filename = csv_filepath + "/" + settings.kCSV_AIA_Wellness_OutputFile

        approved_phone_list = list()
        # open the CSV from the required (local) directory
        with open(csv_filename, newline='') as input_file:
            csv_reader = csv.reader(input_file, delimiter=',')

            # parse row
            for row in csv_reader:
                # pprint(row)
                approved = row[1].strip()
                approved.replace(r'\r', '')

                if approved == "Y":
                    approved_phone = row[0].strip()
                    approved_phone.replace(r'\r', '')
                    pprint("approving Phone Number: " + approved_phone)
                    approved_phone_list.append(row[0].strip())

        pprint("Approved Phone Numbers:")
        pprint(approved_phone_list)
        return approved_phone_list
