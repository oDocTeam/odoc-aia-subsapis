import sys

import requests

sys.path.append('../')
from mongoengine import *
from pprint import pprint
from settings import kLastName, kFirstName, kEmail, kTelephone, kAIAClientsToken,kAIAWellnessToken, kDomainID, kImaadPhoneNumbersKey, \
    kImaadEmailsKey
import helpers.strings
import settings
import json
from models.MongoUser import User
from models.MongoAiaWellnessUser import AiaWellnessUser
from models.StandardResponse import StandardResponse
from services.LogService import LogService


"""
Handle User level functions
"""

# connect to mongo
settings.connect_to_mongo()


# TODO: Make this a Singleton
class UserService(object):
    # The UserService is responsible for:
    # - storing subscription requests, from Sandun's frontend
    # - returning appropriate errors (NIC exists, phone exists, etc) to Sandun's frontend
    # - generating data in the appropriate format (to send to AIA for verification in CSV format)
    # - generating data in the appropriate format (to send to Imaad's backend)
    def __init__(self):
        print("Initialized the UserService.")

    @staticmethod
    def create(payload):
        success_message = None
        error_message = None

        try:
            if (kFirstName in payload and payload[kFirstName]) and (kLastName in payload and payload[kLastName]) and (kTelephone in payload and payload[kTelephone]):
                first_name = payload[kFirstName]
                last_name = payload[kLastName]
                email = payload[kEmail]
                # nic = payload[kNIC]
                telephone = helpers.strings.phone_number_with_plus(payload[kTelephone])

                # check no user exists with this NIC and phone
                user_matched_phone = UserService.get_user_for_telephone_no(telephone_no=telephone)
                # user_matched_nic = UserService.get_user_for_nic(nic=nic)
                if user_matched_phone is not None:
                    success = False
                    error_message = "Could not create user: a user with the phone number " + str(
                        telephone) + " already exists."
                # elif user_matched_nic is not None:
                #     success = False
                #     error_message = "Could not create user: a user with the NIC " + nic + " already exists."
                else:
                    # no user with this phone and NIC exists: create this user
                    user = User(constructor=payload, first_name=first_name, last_name=last_name, telephone_no=telephone ,email=email)
                    user.save()

                    # call Imaad's 'registerphonenumbers' API
                    UserService.add_user_to_domain(user_phone_number=telephone)
                    # call Imaad's 'registeremails' API
                    UserService.add_user_to_email_list(user_phone_number=telephone)

                    success = True
                    success_message = "User successfully created."
            else:
                # create and return the unsuccessful result
                success = False
                error_message = "Could not create user: " + kFirstName + " and/or " + kLastName + " and/or " + kTelephone + " was not passed and/or null."
        except Exception as e:
            success = False
            error_message = "Something really unexpected in UserService->create(). Check logs for error = " + str(e)

        response = StandardResponse(success=success, success_message=success_message, error_message=error_message)
        return response

    @staticmethod
    def create_aia_wellness(payload):
        success_message = None
        error_message = None

        try:
            if (kFirstName in payload and payload[kFirstName]) and (kLastName in payload and payload[kLastName]) and (
                    kTelephone in payload and payload[kTelephone]):
                first_name = payload[kFirstName]
                last_name = payload[kLastName]
                email = payload[kEmail]
                # nic = payload[kNIC]
                telephone = helpers.strings.phone_number_with_plus(payload[kTelephone])

                # check no user exists with this NIC and phone
                user_matched_phone = UserService.get_aia_wellness_user_for_telephone_no(telephone_no=telephone)
                # user_matched_nic = UserService.get_user_for_nic(nic=nic)
                if user_matched_phone is not None:
                    success = False
                    error_message = "Could not create user: a user with the phone number " + str(
                        telephone) + " already exists."
                # elif user_matched_nic is not None:
                #     success = False
                #     error_message = "Could not create user: a user with the NIC " + nic + " already exists."
                else:
                    # no user with this phone and NIC exists: create this user
                    aiaWellnessUser = AiaWellnessUser(constructor=payload, first_name=first_name, last_name=last_name, telephone_no=telephone,
                                email=email)
                    aiaWellnessUser.save()
                    # call Imaad's 'registerphonenumbers' API
                    UserService.add_aia_wellness_user_to_domain(user_phone_number=telephone)
                    # call Imaad's 'registeremails' API
                    UserService.add_aia_wellness_user_to_email_list(user_phone_number=telephone)

                    success = True
                    success_message = "User successfully created."
            else:
                # create and return the unsuccessful result
                success = False
                error_message = "Could not create user: " + kFirstName + " and/or " + kLastName + " and/or " + kTelephone + " was not passed and/or null."
        except Exception as e:
            success = False
            error_message = "Something really unexpected in UserService->create_aia_wellness(). Check logs for error = " + str(e)

        response = StandardResponse(success=success, success_message=success_message, error_message=error_message)
        return response

    @staticmethod
    def get_user_for_telephone_no(telephone_no):
        # query this user's phone number from Mongo
        # remove "+" if present
        # TODO: There should only be one matching user - by DB design
        query_telephone_no = helpers.strings.phone_number_with_plus(telephone_no=telephone_no)
        user = User.objects(telephone_no=query_telephone_no).first()
        return user

    @staticmethod
    def get_aia_wellness_user_for_telephone_no(telephone_no):
        # query this user's phone number from Mongo
        # remove "+" if present
        # TODO: There should only be one matching user - by DB design
        query_telephone_no = helpers.strings.phone_number_with_plus(telephone_no=telephone_no)
        user = AiaWellnessUser.objects(telephone_no=query_telephone_no).first()
        return user


    @staticmethod
    def get_user_for_nic(nic):
        # query this user's nic from Mongo
        # TODO: There should only be one matching user - by DB design
        user = User.objects(nic=nic).first()
        return user

    @staticmethod
    def get_phone_number_for_nic(nic):
        # query this user's nic from Mongo
        # TODO: There should only be one matching user - by DB design
        user = User.objects(nic=nic).first()
        return user.telephone_no if user is not None else None

    @staticmethod
    def get_user_phone_list(delimiter=""):
        # query users phone numbers from Mongo
        users = User.objects()
        return [[user.telephone_no + delimiter ,user.created_date.strftime("%Y-%m-%d %H:%M:%S")] for user in users]

    @staticmethod
    def get_aia_wellness_user_phone_list(delimiter=""):
        # query users phone numbers from Mongo
        users = AiaWellnessUser.objects()
        return [[user.telephone_no + delimiter, user.created_date.strftime("%Y-%m-%d %H:%M:%S")] for user in users]

    @staticmethod
    def get_user_nic_list(delimiter=""):
        # query users NICs from Mongo
        users = User.objects()
        return [user.nic + delimiter for user in users]

    @staticmethod
    def get_user_list():
        # query users from Mongo
        users = User.objects()
        return users

    @staticmethod
    def get_user_list_aia_wellness():
        # query users from Mongo
        users = AiaWellnessUser.objects()
        return users

    @staticmethod
    def dump_database():
        # dump all users from Mongo
        User.objects.delete()

    @staticmethod
    def add_user_to_domain(user_phone_number):
        user_phone_number_list = []
        user_phone_number_list.append(helpers.strings.phone_number_without_plus(user_phone_number))
        # define header
        header = {
            'Content-Type': "application/json",
            'Authorization': "Bearer " + kAIAClientsToken
        }

        # define payload
        payload = {
            kDomainID: settings.kImaadAIADomainID(),
            kImaadPhoneNumbersKey: user_phone_number_list
        }

        pprint("Sending this payload to Imaad:")
        pprint(payload)
        response = requests.request("POST", settings.kURL_add_user_to_domain(), data=json.dumps(payload),
                                    headers=header)

        pprint("Received this response from Imaad:")

        pprint("response = ")
        pprint(response)

        print("\nWhole response = ")
        pprint(json.loads(response.content))

        LogService.log_data(api_name='/add_user_to_domain', request=payload, response=response.json())

    @staticmethod
    def add_aia_wellness_user_to_domain(user_phone_number):
        user_phone_number_list = []
        user_phone_number_list.append(helpers.strings.phone_number_without_plus(user_phone_number))
        # define header
        header = {
            'Content-Type': "application/json",
            'Authorization': "Bearer " + kAIAWellnessToken
        }

        # define payload
        payload = {
            kDomainID: settings.kAIAWellnessDomainID(),
            kImaadPhoneNumbersKey: user_phone_number_list
        }

        pprint("Sending this payload to Imaad:")
        pprint(payload)
        response = requests.request("POST", settings.kURL_add_user_to_domain(), data=json.dumps(payload),
                                    headers=header)

        pprint("Received this response from Imaad:")

        pprint("response = ")
        pprint(response)

        print("\nWhole response = ")
        pprint(json.loads(response.content))

        LogService.log_data(api_name='/add_user_to_domain', request=payload, response=response.json())

    @staticmethod
    def add_user_to_email_list(user_phone_number):
        user_phone_number_list = []
        user_phone_number_list.append(helpers.strings.phone_number_without_plus(user_phone_number))
        # define header
        header = {
            'Content-Type': "application/json",
            'Authorization': "Bearer " + kAIAClientsToken
        }

        # define payload
        payload = {
            kDomainID: settings.kImaadAIADomainID(),
            kImaadEmailsKey: user_phone_number_list
        }

        pprint("Sending this payload to Imaad:")
        pprint(payload)
        response = requests.request("POST", settings.kURL_add_user_to_email_list(), data=json.dumps(payload),
                                    headers=header)

        pprint("Received this response from Imaad  for add_user_to_email_list")

        pprint("response = ")
        pprint(response)

        print("\nWhole response = ")
        pprint(json.loads(response.content))

        LogService.log_data(api_name='/add_user_to_email_list', request=payload, response=response.json())

    @staticmethod
    def add_aia_wellness_user_to_email_list(user_phone_number):
        user_phone_number_list = []
        user_phone_number_list.append(helpers.strings.phone_number_without_plus(user_phone_number))
        # define header
        header = {
            'Content-Type': "application/json",
            'Authorization': "Bearer " + kAIAWellnessToken
        }

        # define payload
        payload = {
            kDomainID: settings.kAIAWellnessDomainID(),
            kImaadEmailsKey: user_phone_number_list
        }

        pprint("Sending this payload to Imaad:")
        pprint(payload)
        response = requests.request("POST", settings.kURL_add_user_to_email_list(), data=json.dumps(payload),
                                    headers=header)

        pprint("Received this response from Imaad  for add_user_to_email_list")

        pprint("response = ")
        pprint(response)

        print("\nWhole response = ")
        pprint(json.loads(response.content))

        LogService.log_data(api_name='/add_user_to_email_list', request=payload, response=response.json())
