from models.MongoLog import MongoLog


# TODO: Make this a Singleton
class LogService(object):

    @staticmethod
    def log_data(api_name, request, response):
        log = MongoLog(API_name=api_name, request=request, response=response)
        log.save()
        return response
