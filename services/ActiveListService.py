import json
from datetime import datetime
from pprint import pprint

import requests
from services.FileService import FileService
from services.LogService import LogService
from services.UserService import UserService

import settings
from models.JsonUser import JsonUser
from settings import kAIAClientsToken, kAIAWellnessToken, kDomainID, kImaadPhoneNumbersKey,kImaadEmailsKey


class ActiveListService(object):
    # this service updates the AIA active list, by:
    # 1 - querying user's phone numbers from Mongo
    # 2 - calling Imaad's API appropriately
    def __init__(self):
        print("Initialized the ActiveListService.")

    @staticmethod
    def update_active_list():
        # ASSUMES: CSV has already been downloaded to local server
        # TODO: write logic to warn and download, if CSV does not exist
        # # fetch approved CSV from shared FTP server with AIA
        # FileService.download_processed_csv()

        # look up corresponding phone numbers for approved NICs
        # approved_nic_list = FileService.extract_approved_nics()
        # user_phone_list = [UserService.get_phone_number_for_nic(nic) for nic in approved_nic_list]

        # look up approved Phone Numbers
        user_phone_list = FileService.extract_approved_phone_numbers()
        user_phone_list_without_plus = []
        # remove + from phone number
        for phone_number in user_phone_list:
            user_phone_list_without_plus.append(phone_number[1:])

        # call Imaad's 'update active list' API
        ActiveListService.update_active_list_with_phone_list(user_phone_list=user_phone_list_without_plus)

    @staticmethod
    def update_active_list_aia_wellness():
        # ASSUMES: CSV has already been downloaded to local server
        # TODO: write logic to warn and download, if CSV does not exist
        # # fetch approved CSV from shared FTP server with AIA
        # FileService.download_processed_csv()

        # look up corresponding phone numbers for approved NICs
        # approved_nic_list = FileService.extract_approved_nics()
        # user_phone_list = [UserService.get_phone_number_for_nic(nic) for nic in approved_nic_list]

        # look up approved Phone Numbers
        user_phone_list = FileService.extract_approved_phone_numbers_aia_wellness()
        user_phone_list_without_plus = []
        # remove + from phone number
        for phone_number in user_phone_list:
            user_phone_list_without_plus.append(phone_number[1:])

        # call Imaad's 'update active list' API
        ActiveListService.update_active_list_with_phone_list_aia_wellness(user_phone_list=user_phone_list_without_plus)

    @staticmethod
    def update_active_email_list():
        # ASSUMES: CSV has already been downloaded to local server
        # TODO: write logic to warn and download, if CSV does not exist
        # # fetch approved CSV from shared FTP server with AIA
        # FileService.download_processed_csv()

        # look up corresponding phone numbers for approved NICs
        # approved_nic_list = FileService.extract_approved_nics()
        # user_phone_list = [UserService.get_phone_number_for_nic(nic) for nic in approved_nic_list]

        # look up approved Phone Numbers
        user_phone_list = FileService.extract_approved_phone_numbers()
        user_phone_list_without_plus = []
        # remove + from phone number
        for phone_number in user_phone_list:
            user_phone_list_without_plus.append(phone_number[1:])

        # call Imaad's 'update active email list' API
        ActiveListService.update_active_email_list_with_phone_list(user_phone_list=user_phone_list_without_plus)


    @staticmethod
    def update_active_email_list_aia_wellness():
        # ASSUMES: CSV has already been downloaded to local server
        # TODO: write logic to warn and download, if CSV does not exist
        # # fetch approved CSV from shared FTP server with AIA
        # FileService.download_processed_csv()

        # look up corresponding phone numbers for approved NICs
        # approved_nic_list = FileService.extract_approved_nics()
        # user_phone_list = [UserService.get_phone_number_for_nic(nic) for nic in approved_nic_list]

        # look up approved Phone Numbers
        user_phone_list = FileService.extract_approved_phone_numbers_aia_wellness()
        user_phone_list_without_plus = []
        # remove + from phone number
        for phone_number in user_phone_list:
            user_phone_list_without_plus.append(phone_number[1:])

        # call Imaad's 'update active email list' API
        ActiveListService.update_active_email_list_with_phone_list_aia_wellness(user_phone_list=user_phone_list_without_plus)

    @staticmethod
    def create_json_file():

        # look up approved Phone Numbers
        approved_phone_list = FileService.extract_approved_phone_numbers()
        user_list = UserService.get_user_list()
        edited_user_list = []

        for user in user_list:
            if user.telephone_no in approved_phone_list:
                approved_user = JsonUser(user.first_name, user.last_name,
                                             user.telephone_no, user.email, "y", user.created_date.strftime("%Y-%m-%d %H:%M:%S"))
                edited_user_list.append(approved_user.__dict__)

            if user.telephone_no not in approved_phone_list:
                unapproved_user = JsonUser(user.first_name, user.last_name,
                                             user.telephone_no, user.email, "n", user.created_date.strftime("%Y-%m-%d %H:%M:%S"))
                edited_user_list.append(unapproved_user.__dict__)

        # generate the JSON filepath and filename
        json_filepath = FileService.get_project_root() + settings.kJSON_Directory
        json_filename = json_filepath + "/" + settings.kJSON_File

        # create directory if it doesn't exist:
        FileService.create_directory_if_not_exists(json_filepath)

        # write the json to the required (local) directory
        with open(json_filename, 'w') as output_file:
            json.dump(edited_user_list, output_file)

    @staticmethod
    def create_json_file_aia_wellness():

        # look up approved Phone Numbers
        approved_phone_list = FileService.extract_approved_phone_numbers_aia_wellness()
        user_list = UserService.get_user_list_aia_wellness()
        edited_user_list = []

        for user in user_list:
            if user.telephone_no in approved_phone_list:
                approved_user = JsonUser(user.first_name, user.last_name,
                                             user.telephone_no, user.email, "y", user.created_date.strftime("%Y-%m-%d %H:%M:%S"))
                edited_user_list.append(approved_user.__dict__)

            if user.telephone_no not in approved_phone_list:
                unapproved_user = JsonUser(user.first_name, user.last_name,
                                             user.telephone_no, user.email, "n", user.created_date.strftime("%Y-%m-%d %H:%M:%S"))
                edited_user_list.append(unapproved_user.__dict__)

        # generate the JSON filepath and filename
        json_filepath = FileService.get_project_root() + settings.kJSON_Directory
        json_filename = json_filepath + "/" + settings.kJSON_AIA_Wellness_File

        # create directory if it doesn't exist:
        FileService.create_directory_if_not_exists(json_filepath)

        # write the json to the required (local) directory
        with open(json_filename, 'w') as output_file:
            json.dump(edited_user_list, output_file)


    @staticmethod
    def update_active_list_with_phone_list(user_phone_list):
        # define header
        header = {
            'Content-Type': "application/json",
            'Authorization': "Bearer " + kAIAClientsToken
        }

        # define payload
        payload = {
            kDomainID: settings.kImaadAIADomainID(),
            kImaadPhoneNumbersKey: user_phone_list
        }

        pprint("Sending this payload to Imaad:")
        pprint(payload)
        response = requests.request("POST", settings.kURL_update_active_list(), data=json.dumps(payload),
                                    headers=header)

        pprint("Received this response from Imaad:")

        pprint("response = ")
        pprint(response)

        print("\nWhole response = ")
        pprint(json.loads(response.content))

        LogService.log_data(api_name='/update_active_list', request=payload, response=response.json())

    @staticmethod
    def update_active_list_with_phone_list_aia_wellness(user_phone_list):
        # define header
        header = {
            'Content-Type': "application/json",
            'Authorization': "Bearer " + kAIAWellnessToken
        }

        # define payload
        payload = {
            kDomainID: settings.kAIAWellnessDomainID(),
            kImaadPhoneNumbersKey: user_phone_list
        }

        pprint("Sending this payload to Imaad:")
        pprint(payload)
        response = requests.request("POST", settings.kURL_update_active_list(), data=json.dumps(payload),
                                    headers=header)

        pprint("Received this response from Imaad:")

        pprint("response = ")
        pprint(response)

        print("\nWhole response = ")
        pprint(json.loads(response.content))

        LogService.log_data(api_name='/update_active_list', request=payload, response=response.json())

    @staticmethod
    def update_active_email_list_with_phone_list(user_phone_list):
        # define header
        header = {
            'Content-Type': "application/json",
            'Authorization': "Bearer " + kAIAClientsToken
        }

        # define payload
        payload = {
            kDomainID: settings.kImaadAIADomainID(),
            kImaadEmailsKey: user_phone_list
        }

        pprint("Sending this payload to Imaad:")
        pprint(payload)
        response = requests.request("POST", settings.kURL_update_active_email_list(), data=json.dumps(payload),
                                    headers=header)

        pprint("Received this response from Imaad for update_active_email_list:")

        pprint("response = ")
        pprint(response)

        print("\nWhole response = ")
        pprint(json.loads(response.content))

        LogService.log_data(api_name='/update_active_email_list', request=payload, response=response.json())

    @staticmethod
    def update_active_email_list_with_phone_list_aia_wellness(user_phone_list):
        # define header
        header = {
            'Content-Type': "application/json",
            'Authorization': "Bearer " + kAIAWellnessToken
        }

        # define payload
        payload = {
            kDomainID: settings.kAIAWellnessDomainID(),
            kImaadEmailsKey: user_phone_list
        }

        pprint("Sending this payload to Imaad:")
        pprint(payload)
        response = requests.request("POST", settings.kURL_update_active_email_list(), data=json.dumps(payload),
                                    headers=header)

        pprint("Received this response from Imaad for update_active_email_list:")

        pprint("response = ")
        pprint(response)

        print("\nWhole response = ")
        pprint(json.loads(response.content))

        LogService.log_data(api_name='/update_active_email_list', request=payload, response=response.json())

    @staticmethod
    def upload_approval_list_to_AIA():
        FileService.generate_approval_csv()
        FileService.upload_approval_csv()

    # to be removed after verification
    @staticmethod
    def add_all_users_to_active_list():
        phone_number_list = UserService.get_user_phone_list()
        ActiveListService.update_active_list_with_phone_list(user_phone_list=phone_number_list)
