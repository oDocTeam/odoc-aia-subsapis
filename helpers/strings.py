import base64
import settings
import json
import random
import string

from datetime import date, datetime

"""
Encrypt and decrypt a string given a key (assumed a phone number for now)
https://stackoverflow.com/questions/2490334/simple-way-to-encode-a-string-according-to-a-password
"""

def encrypt(string_to_encode):
    key = settings.kEncryptionKey
    enc = []
    for i in range(len(string_to_encode)):
        key_c = key[i % len(key)]
        enc_c = chr((ord(string_to_encode[i]) + ord(key_c)) % 256)
        enc.append(enc_c)
    return base64.urlsafe_b64encode("".join(enc).encode()).decode()


def decrypt(encrypted_string):
    try:
        key = settings.kEncryptionKey
        dec = []
        enc = base64.urlsafe_b64decode(encrypted_string).decode()
        for i in range(len(enc)):
            key_c = key[i % len(key)]
            dec_c = chr((256 + ord(enc[i]) - ord(key_c)) % 256)
            dec.append(dec_c)
        return "".join(dec)
    except:
        # could not decrypt
        return None


def generate_random_cookie():
    cookie_length = 20
    cookie = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(cookie_length))
    return cookie.lower()


def phone_number_with_plus(telephone_no):
    return telephone_no if telephone_no[0] == "+" else "+" + telephone_no


def phone_number_without_plus(telephone_no):
    return telephone_no if telephone_no[0] != "+" else telephone_no[1:]


def print_flask_response(flask_request):
    bytes_value = flask_request.data
    json_value = bytes_value.decode('utf8').replace("'", '"')
    print(json_value)
    print('- ' * 20)

    # Load the JSON to a Python list & dump it back out as formatted JSON
    data = json.loads(json_value)
    s = json.dumps(data, indent=4, sort_keys=True)
    print(s)


def json_serial(obj):
    """JSON serializer for objects not serializable by default json code"""

    if isinstance(obj, (datetime, date)):
        return obj.isoformat()
    raise TypeError("Type %s not serializable" % type(obj))
