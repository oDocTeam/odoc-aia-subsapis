import requests
import datetime
from datetime import datetime, timedelta
import time
from pprint import pprint
from services.UserService import UserService
from settings import const_id_prefix, const_sms_endpoint

def generate_sms_for_new_users():
    user_list = UserService.get_user_list()
    d = datetime.now() - timedelta(days=1)

    # Send sms alert to Praveen
    sms_sender("+94773521042", "AIA registration SMS alert started now.")

    no_of_sms_sent = 0
    for user in user_list:
        if d.strftime("%Y-%m-%d-%H:%M:%S")< user.created_date.strftime("%Y-%m-%d-%H:%M:%S"):
            no_of_sms_sent = no_of_sms_sent + 1
            print("SMS sent to phone number : "+user.telephone_no)
            sms_sender(user.telephone_no,"oDoc is brought to you by AIA Insurance. If you haven't downloaded the oDoc app, visit www.odoc.life/get now to download. For more information call 0770773333")
            time.sleep(10)

    # Send sms alert to Ashik
    sms_sender("+94773830877",
               "AIA registration SMS alert has been triggered. No of sms sent : " + str(no_of_sms_sent))
    # Send sms alert to Praveen
    sms_sender("+94773521042",
               "AIA registration SMS alert has been triggered. No of sms sent : " + str(no_of_sms_sent))


def sms_sender(mobile_number, text):
    payload = generate_sms_payload(mobile_number=mobile_number, text=text)
    send_sms(payload)


def generate_sms_payload(mobile_number, text):
    datetime_string = datetime.now().strftime("%Y-%m-%d-%H:%M:%S")
    message_id = const_id_prefix + datetime_string
    return {
        "message": {
            "id": message_id,
            "toTelNo": mobile_number,
            "senderName": "oDoc",
            "body": text,
            "ttl": 120000,
            "priority": "low"
        }
    }

def send_sms(payload):
    pprint(payload)
    r = requests.post(url=const_sms_endpoint, json=payload)
    response = r.json()
    pprint("Response was:")
    pprint(response)


generate_sms_for_new_users()