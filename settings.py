from mongoengine import *
from config import kIsDevelopment

# Flask settings
flask_port = 6006


# connect to mongoDB depending on deployment location
def connect_to_mongo():
    if kIsDevelopment:
        connect('odoc-aia')
    else:
        connect('odoc-aia', username='odoc-aia-admin', password='odocaia-asft79#H$L')


kAIAClientsToken = 'wwQcT5b4lsHLdBjuy4SdKrGO6mehgMvalUgxqiZ6YedEe'
kAIAWellnessToken = 'FGjVd6xFok0hf9ZIUrct0sCobNBL9nWx5cBWnuieYAeuj'

# TODO: add if/then to set domain ID based on dev/live
kImaadPhoneNumbersKey = 'phoneNumbers'
kImaadEmailsKey = 'emails'
kDomainID = 'id'


def kImaadAIADomainID():
    if kIsDevelopment:
        return "56"
    else:
        return "109"

def kAIAWellnessDomainID():
    if kIsDevelopment:
        return "5207"
    else:
        return "313"


def kURL_update_active_list():
    if kIsDevelopment:
        return "https://dev.gen2.odoc.life/b2bdashboard/updateactivephonelist"
    else:
        return "https://gen2.odoc.life/b2bdashboard/updateactivephonelist"


def kURL_update_active_email_list():
    if kIsDevelopment:
        return "https://dev.gen2.odoc.life/b2bdashboard/updateactiveemaillist"
    else:
        return "https://gen2.odoc.life/b2bdashboard/updateactiveemaillist"


def kURL_add_user_to_domain():
    if kIsDevelopment:
        return "https://dev.gen2.odoc.life/b2bdashboard/registerphonenumbers"
    else:
        return "https://gen2.odoc.life/b2bdashboard/registerphonenumbers"


def kURL_add_user_to_email_list():
    if kIsDevelopment:
        return "https://dev.gen2.odoc.life/b2bdashboard/registeremails"
    else:
        return "https://gen2.odoc.life/b2bdashboard/registeremails"


# filenames and  directory constants
kCSV_Directory = '/csv'
kCSV_AIA_InputFile = 'ODOC_INPUT.csv'
kCSV_AIA_Wellness_InputFile = 'ODOC_AIA_WELLNESS_INPUT.csv'
kCSV_AIA_InputFile_archived = 'ODOC_INPUT.zip'
kCSV_AIA_Wellness_InputFile_archived = 'ODOC_AIA_WELLNESS_INPUT.zip'
kCSV_AIA_OutputFile = 'ODOC_OUTPUT.CSV'
kCSV_AIA_Wellness_OutputFile = 'ODOC_AIA_WELLNESS_OUTPUT.CSV'
kCSV_AIA_OutputFile_archived = 'ODOC_OUTPUT.ZIP'
kCSV_AIA_Wellness__OutputFile_archived = 'ODOC_AIA_WELLNESS_OUTPUT.ZIP'
kJSON_Directory = '/json'
kJSON_File = 'AIA_DATA.json'
kJSON_AIA_Wellness_File = 'AIA_WELLNESS_DATA.json'
kCSV_UploadScript = 'upload_folder.sh'
kCSV_DownloadScript = 'download_folder.sh'

# user constants
kFirstName = 'first_name'
kLastName = 'last_name'
kTelephone = 'telephone_no'
kNIC = 'nic'
kEmail = 'email'

# keys for path
const_sms_endpoint = 'https://gen2.odoc.life/sms/send'
const_id_prefix = 'SMSSender-'

